import {ReactKeycloakProvider} from "@react-keycloak/web";
import keycloak from "./components/auth/Keycloak";
import React from "react";
import AppRouter from "./components/AppRouter";

function App() {
    return (
        <ReactKeycloakProvider authClient={keycloak}>
            <AppRouter/>
       </ReactKeycloakProvider>
    );
}

export default App;