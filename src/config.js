// KEYCLOAK CONFIG
export const keycloakUrl = "http://localhost:8080/auth";
export const keycloakRealm = "polyglot-auth";
export const keycloakClientId = "polyglot";

// URL
export const root = "/"
export const accountUrl = keycloakUrl + "/realms/" + keycloakRealm + "/account"
export const adminUrl = keycloakUrl + "/admin/master/console"
export const appPageUrl = "/app"
export const errorUrl = "/error"
export const notebookPatternUrl = appPageUrl + "/notebook/*"
export const notebookUrl = appPageUrl + "/notebook"

// API
export const apiUrl = "http://localhost:5000/api"
export const userApiUrl = apiUrl + "/user"


export const axiosConfig = {
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
}