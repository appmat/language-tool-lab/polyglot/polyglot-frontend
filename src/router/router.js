import React from "react";
import AppPage from "../components/pages/AppPage";
import ErrorPage from "../components/pages/ErrorPage";
import HomePage from "../components/pages/HomePage";
import {appPageUrl, errorUrl, root, notebookPatternUrl} from "../config";

export const privateRoutes = [
    {path: appPageUrl, element: AppPage, exact: true},
    {path: notebookPatternUrl, element: AppPage, exact: true}
]

export const publicRoutes = [
    {path: root, element: HomePage, exact: true},
    {path: errorUrl, element: ErrorPage, exact: true},
]