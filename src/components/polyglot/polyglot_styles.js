export const leftShellStyle = {
    gridArea: "left / left / left / left",
}
export const dragHandleLeftStyle = {
    area: "leftdrag / leftdrag / leftdrag / leftdrag"
}
export const dragHandleRightStyle = {
    area: "rightdrag / rightdrag / rightdrag / rightdrag"
}
export const rightShellStyle = {
    gridArea: "right / right / right / right",
}

export const noAuthButtonStyle = {
    padding: "8px",
    borderRadius: "6px",
}