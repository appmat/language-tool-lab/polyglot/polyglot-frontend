
export const getNotebookId = (path) => {
    if (path.includes("notebook")) {
        const index = path.lastIndexOf("/");
        if (index !== -1) {
            return path.substring(index + 1);
        }
        return "";
    }
    return "0"
}

export const getInsertIndex = (pid, paragraphs) => {
    let index = 0
    for (let i = 0; i < paragraphs.length; i++) {
        let currentParagraphKey = getParagraphId(paragraphs[i])
        if (currentParagraphKey == pid) {
            index = i;
            break;
        }
    }
    return index;
}

export const getFreeId = (list) => {
    let freeId
    let mx = 0
    if (list.length === 0) {
        mx = -1
    } else {
        for (let i = 0; i < list.length; i++) {
            let pid = getParagraphId(list[i])
            let cid = parseInt(pid)
            if (cid > mx) {
                mx = cid
            }
        }
    }
    freeId = mx + 1
    return freeId;
}

export const getParagraphId = (str) => {
    let j = JSON.stringify(str)
    const map = new Map(Object.entries(JSON.parse(j)));
    return map.keys().next().value
}

export const getParagraphData = (str) => {
    let j = JSON.stringify(str)
    const map = new Map(Object.entries(JSON.parse(j)));
    return  map.values().next().value
}