import React, {useState} from "react";
import MyButton from "../../ui/MyButton";
import axios from "axios";
import {apiUrl, axiosConfig} from "../../../config";
import keycloak from "../../auth/Keycloak";
import {getFreeId} from "../utils";
import {clearIconButton, killIconButton, startIconButton} from "../button_codes";

function Kernel(props) {

    const [isKernelStarted, setIsKernelStarted] = useState(false)
    const [isKernelKilled, setIsKernelKilled] = useState(false)

    const startKernel = async(event) => {
        let task = {}
        event.target.style.color = "yellow"

        if (!isKernelStarted) {
            axiosConfig.headers["Authorization"] = `Bearer ${keycloak.token}`
            await axios.post(`${apiUrl}/notebook/${props.notebookid}/kernel/start`, axiosConfig)
                .then(() => {
                    let id = String(getFreeId(props.kernelTasks))
                    task[id] = "Kernel is started"
                    props.addKernelTask(task)

                    setIsKernelStarted(true)
                    event.target.style.color = "green"
                }).catch(err => {
                    let id = String(getFreeId(props.kernelTasks))
                    let message = err.message
                    if (typeof err.response !== "undefined") {
                        message = err.message + "(" + err.response.data + ")"
                    }
                    task[id] = message
                    props.addKernelTask(task)

                    setIsKernelStarted(false)
                    event.target.style.color = "red"
                })
        } else {
            event.target.style.color = "green"
        }
    }

    const killKernel = async(event) => {
        let startKernelButton = document.querySelector(".kernel-header .start")
        let task = {}
        event.target.style.color = "yellow"

        if (!isKernelKilled) {
            axiosConfig.headers["Authorization"] = `Bearer ${keycloak.token}`
            await axios.post(`${apiUrl}/notebook/${props.notebookid}/kernel/kill`, axiosConfig)
                .then(() => {
                    let id = String(getFreeId(props.kernelTasks))
                    task[id] = "Kernel was killed"
                    props.addKernelTask(task)

                    event.target.style.color = "green"
                    setIsKernelKilled(true)

                    setTimeout(() => {
                        setIsKernelStarted(false)
                        setIsKernelStarted(false)
                        event.target.style.color = ""
                        startKernelButton.style.color = ""
                    }, 5000);
                }).catch(err => {
                    let id = String(getFreeId(props.kernelTasks))
                    let message = err.message
                    if (typeof err.response !== "undefined") {
                        message = err.message + "(" + err.response.data + ")"
                    }
                    task[id] = message
                    props.addKernelTask(task)

                    startKernelButton.style.color = "red"
                    event.target.style.color = "red"
                    setIsKernelKilled(false)
                    setIsKernelStarted(false)
                })
        } else {
            event.target.style.color = "green"
        }
    }

    const clearAllTasks = () => {
        props.kernelTasks.forEach(task => {
            let taskId = Object.keys(task)[0]
            props.removeKernelTask(taskId)
        })
    }

    const splitViewComponent = document.querySelector(".split-view")
    const [collapsed, setCollapsed] = useState(false)

    const collapse = (e) => {
        if (!e.target.className.toString().includes("button") && !(e.target.className instanceof SVGAnimatedString)) {
            setCollapsed(!collapsed)
            if (collapsed) {
                splitViewComponent.classList.remove("right-collapsed")
            } else {
                splitViewComponent.classList.add("right-collapsed")
            }
        }
    }

    return (
        <div className="ui-panel">
            <h2 className="ui-panel-header kernel-header" onClick={collapse}>
                Kernel
                <span className="buttons">
                    <MyButton
                        className="start icon-button"
                        onClick={startKernel}
                        title="Start kernel"
                        viewBox="0 0 512 512"
                        alt="Start"
                        path={startIconButton}
                    />
                    <MyButton
                        className="kill icon-button"
                        onClick={killKernel}
                        title="Kill kernel"
                        viewBox="0 0 512 512"
                        alt="Kill"
                        path={killIconButton}
                    />
                    <MyButton
                        className="clear icon-button"
                        onClick={clearAllTasks}
                        title="Clear tasks"
                        viewBox="0 0 512 512"
                        alt="Clear"
                        path={clearIconButton}
                    />
                </span>
            </h2>
            <div className="kernel-stack">
                {
                    props.kernelTasks.map((task, index) => (
                        <KernelTask
                            key={index}
                            text={task[Object.keys(task)[0]]}
                        />
                    ))
                }
            </div>
        </div>
    );
}

function KernelTask(props) {
    return (
        <div className="kernel-task">
            <h1> {props.text} </h1>
        </div>
    )
}

export default Kernel;