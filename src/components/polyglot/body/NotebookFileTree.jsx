import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import {apiUrl, axiosConfig, notebookUrl} from "../../../config";
import axios from "axios";
import {useKeycloak} from "@react-keycloak/web";
import {dataSplitter} from "../../../data";
import {getFreeId} from "../utils";

function NotebookFileTree(props) {

    const loc = useLocation();
    let navigate = useNavigate();
    const { keycloak } = useKeycloak();

    const openNotebook = async () => {
        let notebookId = props.id
        let response = ""
        axiosConfig.headers["Authorization"] = `Bearer ${keycloak.token}`
        await axios.get(`${apiUrl}/notebook/${notebookId}`, axiosConfig)
            .then(res => {response = res})
            .catch(ex => {
                let task = {}
                let id = String(getFreeId(props.kernelTasks))
                task[id] = "Failed to open notebook: " + ex
                props.addKernelTask(task)
                return null
            })
        return response
    }

    async function onNotebookOpened() {
        const openedNotebook = await openNotebook()
        if (openedNotebook.data != null) {
            let notebookID = openedNotebook.data.id
            if (loc.pathname.includes("notebook")) {
                navigate(notebookUrl + "/" + notebookID);
            } else {
                navigate(`${loc.pathname.at(-1) === '/' ? loc.pathname.slice(0, -1) : loc.pathname}/notebook/${notebookID}`)
            }
            let receivedParagraphs = []
            for (let paragraphIndex in openedNotebook.data.paragraphs) {
                let paragraph = {}
                let id = openedNotebook.data.paragraphs[paragraphIndex].id
                let language = openedNotebook.data.paragraphs[paragraphIndex].language
                let code = openedNotebook.data.paragraphs[paragraphIndex].code
                let output = openedNotebook.data.paragraphs[paragraphIndex].output
                paragraph[id] = language + dataSplitter +
                    code + dataSplitter + output
                receivedParagraphs.push(paragraph)
            }
            props.setNotebookid(notebookID)
            props.setParagraphs(receivedParagraphs)
        }
    }

    return (
        <div className="notebook-file" onClick={onNotebookOpened}>
            <span className="" id={props.id}> {props.name}.ipynb</span>
        </div>
    )
}

export default NotebookFileTree;