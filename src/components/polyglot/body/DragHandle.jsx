import React, { useRef } from "react";

// Компонент, отвечающий за горизонтальную линию, разделяющую компоненты
function DragHandle(props) {

    const dragHandleRef = useRef(null);
    const innerRef = useRef(null);

    return (
        <div ref={dragHandleRef} className={'drag-handle ' + props.side}
             style={{ gridArea: props.area }}>
            <div ref={innerRef} className="inner"/>
        </div>
    );
}

export default DragHandle;