import React from "react";
import ParagraphContainer from "./ParagraphContainer";

function ParagraphComponent(props) {

    function handleOnClick(event) {
        event.preventDefault();
        const paragraphContainers =
            document.querySelectorAll(".paragraph-component");
        let targetParagraphContainer = null
        document.querySelectorAll(".paragraph-component")
            .forEach(paragraph => {
                if (paragraph.id == props.id)
                    targetParagraphContainer = paragraph
        });
        if (targetParagraphContainer != null) {
            for (let i = 0; i < paragraphContainers.length; i++) {
                if (paragraphContainers[i] === targetParagraphContainer) {
                    paragraphContainers[i].classList.add("active");
                } else {
                    paragraphContainers[i].classList.remove("active");
                }
            }
        }
    }

    return (
        <div className="paragraph-component"
             id={props.id}
             onClick={handleOnClick}>
            <ParagraphContainer
                id={props.id}
                paragraphdata={props.paragraphdata}
                notebookid={props.notebookid}/>
            <div className="paragraph-dragger" {...props}>
                <div className="inner"/>
            </div>
        </div>
    );
}

export default ParagraphComponent;