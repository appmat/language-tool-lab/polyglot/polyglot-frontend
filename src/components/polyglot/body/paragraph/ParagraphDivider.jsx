import React from "react";
import axios from "axios";
import {apiUrl, axiosConfig} from "../../../../config";
import keycloak from "../../../auth/Keycloak";
import {getFreeId, getInsertIndex} from "../../utils";
import {dataDivider} from "../../../../data";

// Компонент, отвечающий за создание параграфа через разделяющий элемень
function ParagraphDivider(props) {

    // создаём параграф, получаем свободный ID
    const getFreeParagraphId = async () => {
        try {
            axiosConfig.headers["Authorization"] = `Bearer ${keycloak.token}`
            let response = await axios.post(
                    `${apiUrl}/notebook/${props.notebookid}/paragraph/`,
                    axiosConfig
            )
            return await response.data
        } catch (ex) {
            let task = {}
            let id = String(getFreeId(props.kernelTasks))
            task[id] = "Failed to create paragraph using +: " + ex
            props.addKernelTask(task)
            return getFreeParagraphIdWithError()
        }
    }

    const getFreeParagraphIdWithError = () => {
        let freeId
        let mx = 0
        if (props.paragraphs.length === 0) {
            mx = -1
        } else {
            for (let i = 0; i < props.paragraphs.length; i++) {
                let pid = parseInt(props.paragraphs[i].id)
                if (pid > mx) {
                    mx = pid
                }
            }
        }
        freeId = mx + 1
        return freeId
    }

    async function onInsertParagraphUsingDivider(e) {
        e.preventDefault()
        let id = await getFreeParagraphId()
        let newParagraph = {}
        newParagraph[id] = dataDivider
        const idx = getInsertIndex(e.target.i, props.paragraphs) + 1;
        props.insertParagraph(idx, newParagraph);
    }

    return (
        <div className="new-paragraph-divider"
             onClick={onInsertParagraphUsingDivider}
             id={props.id}>
        </div>
    );
}

export default ParagraphDivider;