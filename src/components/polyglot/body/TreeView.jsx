import React from "react";
import NotebookFileTree from "./NotebookFileTree";

function TreeView(props) {
    return (
        <div className="tree-view">
            <ul className="">
                <li className="branch">
                    <button className="branch-outer" type="button">
                        <span className="expander"/>
                        <span className="icon"/>
                        <span className="name"> directory </span>
                    </button>

                </li>

                <li className="leaf">
                    <a className="name">
                        {
                            Object.entries(props.notebooks).map(([key, value]) =>
                                <NotebookFileTree
                                    key={key}
                                    id={key}
                                    name={value}
                                    notebookid={props.notebookid}
                                    setNotebookid={props.setNotebookid}
                                    notebooks={props.notebooks}
                                    setNotebooks={props.setNotebooks}
                                    paragraphs={props.paragraphs}
                                    setParagraphs={props.setParagraphs}
                                    kernelTasks={props.kernelTasks}
                                    addKernelTask={props.addKernelTask}
                                />
                            )
                        }
                    </a>
                </li>
            </ul>
        </div>
    );
}

export default TreeView;