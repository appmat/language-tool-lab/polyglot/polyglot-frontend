import React from "react";

import {DragDropContext, Draggable, Droppable} from "react-beautiful-dnd";
import ParagraphDivider from "./paragraph/ParagraphDivider";
import ParagraphComponent from "./paragraph/ParagraphComponent";
import {getParagraphData, getParagraphId} from "../utils";
//
function Notebook(props) {

    // перемещение параграфов
    const onDragEndEvent = (param) => {
        const srcI = param.source.index;
        if (param.destination != null) {
            const desI = param.destination.index;
            if (desI) {
                props.paragraphs.splice(desI, 0, props.paragraphs.splice(srcI, 1)[0]);
                props.setParagraphs(props.paragraphs);
            }
        }
    }
    
    return (
        <div className="notebook-content">
            <div className="notebook-paragraphs">
                <ParagraphDivider
                    key="-1" id="-1"
                    notebookid={props.notebookid}
                    paragraphs={props.paragraphs}
                    createParagraph={props.createParagraph}
                    insertParagraph={props.insertParagraph}
                />

                <DragDropContext onDragEnd={onDragEndEvent}>
                    <Droppable droppableId="droppable">
                        {provided => (
                            <div ref={provided.innerRef} {...provided.droppableProps}>
                            {
                            props.paragraphs.map((paragraph, index) => (
                            <Draggable
                                key={getParagraphId(paragraph)}
                                draggableId={"draggable-" + getParagraphId(paragraph)}
                                index={index}>

                                {(provided) => (
                                <div {...provided.draggableProps}
                                     ref={provided.innerRef}>
                                    <div className="paragraph-and-divider"
                                         id={getParagraphId(paragraph)}>
                                        <ParagraphComponent
                                            id={getParagraphId(paragraph)}
                                            notebookid={props.notebookid}
                                            paragraphdata={getParagraphData(paragraph)}
                                            {...provided.dragHandleProps}/>
                                        <ParagraphDivider
                                            id={getParagraphId(paragraph)}
                                            notebookid={props.notebookid}
                                            paragraphs={props.paragraphs}
                                            insertParagraph={props.insertParagraph}
                                            kernelTasks={props.kernelTasks}
                                            addKernelTask={props.addKernelTask}
                                        />
                                    </div>
                                </div>
                                )}
                            </Draggable>
                            ))
                            }
                            {provided.placeholder}
                            </div>
                        )}
                    </Droppable>
                </DragDropContext>
            </div>
        </div>
    );
}

export default Notebook;