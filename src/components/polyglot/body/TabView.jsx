import React, {useEffect, useState} from "react";
import Notebook from "./Notebook";
import {useKeycloak} from "@react-keycloak/web";
import axios from "axios";
import {apiUrl, appPageUrl, axiosConfig} from "../../../config";
import {defaultNotebooks} from "../../../data";
import LoadingPage from "../../pages/LoadingPage";
import {useLocation, useNavigate} from "react-router-dom";

function TabView(props) {

    const { keycloak } = useKeycloak();
    let navigate = useNavigate();
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        // загружаем все ноутбуки
        const loadAllNotebooks = async() => {
            setLoading(true);
            try {
                axiosConfig.headers["Authorization"] = `Bearer ${keycloak.token}`
                const response = await axios.get(`${apiUrl}/notebook/all`, axiosConfig)
                const dataNotebooks = await response.data
                let notebooks = {}
                for (let notebookIndex in dataNotebooks) {
                    let id = dataNotebooks[notebookIndex].id
                    notebooks[id] = dataNotebooks[notebookIndex].title
                }
                props.setNotebooks(notebooks)
            } catch (error) {
                props.setNotebooks(defaultNotebooks)
            }
            navigate(appPageUrl)
        }
        loadAllNotebooks().then(() => setLoading(false))
    }, []);

    return (
        <div className="">
            {loading ?
                <LoadingPage/> :
                <TabViewContent
                    notebookid={props.notebookid}
                    setNotebooks={props.setNotebooks}
                    paragraphs={props.paragraphs}
                    createParagraph={props.createParagraph}
                    insertParagraph={props.insertParagraph}
                    setParagraphs={props.setParagraphs}

                    kernelTasks={props.kernelTasks}
                    addKernelTask={props.addKernelTask}
                />
            }
        </div>
    );
}

function TabViewContent(props) {
    const location = useLocation();
    return (
        <div className="tab-view">
            <div className="tabbed-pane tab-container"> </div>
            <div className="vim-status hide"> <div className="status"/> </div>
            {location.pathname.includes("notebook") ?
                <Notebook notebookid={props.notebookid}
                          paragraphs={props.paragraphs}
                          createParagraph={props.createParagraph}
                          insertParagraph={props.insertParagraph}
                          setParagraphs={props.setParagraphs}

                          kernelTasks={props.kernelTasks}
                          addKernelTask={props.addKernelTask}
                /> : <div className="welcome-page"> WELCOME PAGE </div>
            }
        </div>
    )
}

export default TabView;