import React from "react";
import MyButton from "../../ui/MyButton";
import {getParagraphData, getParagraphId} from "../utils";
import {dataSplitter} from "../../../data";
import {clearCode, downloadCode, runAllCode, stopCode} from "../button_codes";

function NotebookHeader(props) {

    function onRunAll() {
        const paragraphButtons = document.querySelectorAll(".run-paragraph");
        paragraphButtons.forEach(paragraphButton => {
            paragraphButton.click();
        });
    }

    function onClearAll() {
        const paragraphButtons = document.querySelectorAll(".clear-output");
        paragraphButtons.forEach(paragraphButton => {
            paragraphButton.click();
        });
    }

    function onDownload() {
        let exportData = {
            id: "", title: "", paragraphs: [
                {id: "", language: "", code: "", out: ""}
            ]
        }
        exportData.id = props.notebookid
        exportData.title = props.notebooks[props.notebookid]
        let paragraphs = []
        for (let paragraphIndex in props.paragraphs) {
            let paragraph = {id: "", language: "", code: "", out: ""}
            let paragraphStr = props.paragraphs[paragraphIndex]
            let paragraphId = getParagraphId(paragraphStr)
            let paragraphData = getParagraphData(paragraphStr)
            const [lang, code, out] = paragraphData.split(dataSplitter)

            paragraph.id = paragraphId
            paragraph.language = lang
            paragraph.code = code
            paragraph.out = out
            paragraphs.push(paragraph)
        }
        exportData.paragraphs = paragraphs
        const dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportData));
        const downloadAnchorNode = document.createElement('a');
        downloadAnchorNode.setAttribute("href", dataStr);
        downloadAnchorNode.setAttribute("download", "polyglot.json");
        document.body.appendChild(downloadAnchorNode);
        downloadAnchorNode.click();
        downloadAnchorNode.remove();
    }

    return (
        <div className="toolbar notebook">
            <h3> Notebook </h3>
            <div className="tool-group">
                <MyButton
                    className="run-paragraph run-all icon-button"
                    title="Run all paragraphs"
                    viewBox="0 0 512 512"
                    alt="Run all"
                    onClick={onRunAll}
                    path={runAllCode}
                />

                <MyButton
                    className="stop-paragraph icon-button"
                    title="Cancel all tasks"
                    viewBox="0 0 448 512"
                    alt="Cancel All"
                    path={stopCode}
                />

                <MyButton
                    className="download icon-button"
                    title="Download"
                    viewBox="0 0 512 512"
                    alt="Download"
                    onClick={onDownload}
                    path={downloadCode}
                />

                <MyButton
                    className="clear icon-button"
                    title="Clear notebook outputs"
                    viewBox="0 0 512 512"
                    alt="Clear"
                    onClick={onClearAll}
                    path={clearCode}
                />
        </div>
    </div>
    );
}

export default NotebookHeader;