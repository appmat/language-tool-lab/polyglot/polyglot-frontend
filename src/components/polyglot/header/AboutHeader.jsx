import { useNavigate } from "react-router-dom";
import React, {useEffect, useState} from "react";
import MyButton from "../../ui/MyButton";
import {root} from "../../../config";
import {Button, Checkbox, Modal} from "antd";
import {FormControlLabel, FormGroup} from "@mui/material";
import {configCode, exitCode} from "../button_codes";

function AboutHeader(props) {
    const navigate = useNavigate();
    const [open, setOpen] = useState(false);
    const [autoSaving, setAutoSaving] = useState(false);

    // каждые 5 минут будет происходить обновление ноутбука, если
    // заданы соответствующие настройки
    useEffect(() => {
        const interval = setInterval(() => {
            if (autoSaving) {
                props.saveNotebook()
            }
        }, 300000);
        return () => clearInterval(interval)
    }, []);

    const showModal = () => {
        setOpen(true);
    };

    const handleChange = (event) => {
        setAutoSaving(event.target.checked);
    };

    return (
        <div className="toolbar about">
            <h3 className=""> About </h3>
            <div className="tool-group">
                <MyButton
                    className="about-setting icon-button"
                    title="View UI Settings"
                    viewBox="0 0 640 512"
                    alt="Settings"
                    path={configCode}
                    onClick={showModal}
                />

                <Modal
                    bodyStyle={{ height: 50 }}
                    open={open}
                    className="notebook-modal notebook-modal-settings"
                    title="Settings"
                    onCancel={() => setOpen(false)}
                    footer={[
                        <div className="buttons ">
                            <Button key="save-btn" onClick={() => setOpen(false)}>
                                SAVE
                            </Button>
                        </div>
                    ]}>
                    <FormGroup className="checkboxes" style = {{
                        textAlign: "center",
                        justifyContent: "space-around"
                    }}>
                        <FormControlLabel
                            style = {{
                                fontFamily: "\"Bungee\", serif",
                                marginLeft: "auto",
                                marginRight: "auto",
                                fontSize: 10,
                            }}
                            control={
                            <Checkbox checked={autoSaving}
                                      onChange={handleChange}
                                      style={{ paddingRight: 5 }}/>
                            }
                            label="Auto saving"
                        />
                    </FormGroup>
                </Modal>

                <MyButton
                    className="about-exit icon-button"
                    onClick={() => navigate(root)}
                    title="Exit"
                    viewBox="0 0 495.398 495.398"
                    alt="Exit"
                    path={exitCode}
                />

            </div>
        </div>
    );
}

export default AboutHeader;