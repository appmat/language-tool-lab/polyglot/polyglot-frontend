import React, {useState} from "react";
import MyButton from "../../ui/MyButton";
import MySelect from "../../ui/MySelect";
import {getFreeId, getInsertIndex, getParagraphId} from "../utils";
import {dataDivider} from "../../../data";
import {deleteParagraphCode, insertParagraphAboveCode, insertParagraphBelowCode} from "../button_codes";

function ParagraphLanguage() {
    const [selected, setSelected] = useState("python");

    function setLanguage(lang) {
        setSelected(lang);
    }
    return (
        <div className={"paragraph-language dropdown " + selected}>
            <MySelect
                id="paragraph-language-dropdown"
                selectedValue={selected}
                onChange={setLanguage}
                options={[
                    {value: "python", name: "Python"},
                    {value: "kotlin", name: "Kotlin"},
                    {value: "java", name: "Java"},
                    {value: "scala", name: "Scala"},
                    {value: "javascript", name: "Javascript"},
                ]}
            />
        </div>
    );
}

function ParagraphHeader(props) {

    async function onRemoveParagraph() {
        const activeParagraph = document.querySelector(".paragraph-component.active");
        let pid = -1
        if (activeParagraph !== null) {
            pid = activeParagraph.id
        } else if (props.paragraphs.length !== 0) {
            pid = getParagraphId(props.paragraphs.slice(-1).pop())
        } else {
            let task = {}
            let id = String(getFreeId(props.kernelTasks))
            task[id] = "All paragraphs deleted"
            props.addKernelTask(task)
            return
        }
        props.deleteParagraph(pid);
    }

    async function onInsertParagraphAbove() {
        const id = getFreeId(props.paragraphs);
        const activeParagraph = document.querySelector(".paragraph-component.active");

        const newParagraph = {}
        newParagraph[id] = dataDivider
        // в начало списка
        if (activeParagraph == null) {
            props.insertParagraph(0, newParagraph)
            // перед найденным
        } else {
            const idx = getInsertIndex(activeParagraph.id, props.paragraphs);
            if (idx == 0)
                props.insertParagraph(0, newParagraph)
            else
                props.insertParagraph(idx, newParagraph)
        }
    }

    async function onInsertParagraphBelow() {
        const id = getFreeId(props.paragraphs);
        const activeParagraph = document.querySelector(".paragraph-component.active");

        const newParagraph = {}
        newParagraph[id] = dataDivider

        // в конец списка
        if (activeParagraph == null) {
            props.insertParagraph(id, newParagraph)
            // после найденного
        } else {
            props.insertParagraph(getInsertIndex(activeParagraph.id, props.paragraphs)+1, newParagraph)
        }
    }

    return (
        <div className="toolbar paragraph">
            <h3 className=""> paragraph </h3>
            <div className="tool-group">
                <ParagraphLanguage/>
            </div>
            <div className="tool-group">
                <MyButton
                    className="insert-paragraph-above icon-button"
                    title="Insert paragraph above current"
                    viewBox="0 0 448 512"
                    alt="Insert above"
                    onClick={onInsertParagraphAbove}
                    path={insertParagraphAboveCode}
                />

                <MyButton
                    className="insert-paragraph-below icon-button"
                    title="Insert paragraph below current"
                    viewBox="0 0 448 512"
                    alt="Insert below"
                    onClick={onInsertParagraphBelow}
                    path={insertParagraphBelowCode}
                />

                <MyButton
                    className="delete-paragraph icon-button"
                    onClick={onRemoveParagraph}
                    title="Delete current paragraph"
                    viewBox="0 0 448 512"
                    alt="Delete"
                    path={deleteParagraphCode}
                />

            </div>
        </div>
    );
}

export default ParagraphHeader;