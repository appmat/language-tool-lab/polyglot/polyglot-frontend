import React from "react";
import Editor from "@monaco-editor/react";

function MyMonacoEditor(props) {

    return (
        <Editor
            height="15vh"
            language={props.lang}
            value={props.code}
            theme="vs-dark"
            onChange={event => props.setCode(event)}
            options={{
                // широкая линейка (полоса прокрутки)
                minimap: { enabled: false },
                selectOnLineNumbers: false,
                fontWeight: "bold",
                automaticLayout: true,
                codeLens: false,
                dragAndDrop: true,
                scrollbar: {
                    verticalScrollbarSize: 0,
                    alwaysConsumeMouseWheel: false,
                    vertical: "hidden",
                },
                lineNumbersMinChars: 1,
                lineDecorationsWidth: 0,
                parameterHints: { enabled: true },
                renderLineHighlight: "none",
                scrollBeyondLastLine: false,
                fixedOverflowWidgets: true,
                lineNumbers: 'on',
            }}
        />
    );
}

export default MyMonacoEditor;