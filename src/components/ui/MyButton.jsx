import React from 'react';

const MyButton = ({children, ...props}) => {
    return (
        <button {...props}
                className={props.className}
                title={props.title}
                type="button">
            <span className="">
                <svg xmlns="http://www.w3.org/2000/svg"
                     fill="currentColor"
                     viewBox={props.viewBox}
                     className="icon"
                     alt={props.alt}>
                    <path d={props.path}/>
                    {children}
                </svg>
            </span>
        </button>
    );
};

export default MyButton;