import React from 'react';
import {accountUrl, adminUrl, axiosConfig, appPageUrl, userApiUrl} from "../../config";
import { useKeycloak } from "@react-keycloak/web";
import axios from "axios";

function HomeToolbar() {
    require("../../assets/HomePage.css")
    const { keycloak } = useKeycloak();

    const register = async() => {
        try {
            let keycloakUser = {
                authenticated: keycloak.authenticated,
                clientId: keycloak.clientId,
                realm: keycloak.realm,
                subject: keycloak.subject,
                idToken: keycloak.idToken,
                sessionId: keycloak.sessionId,
            }
            axiosConfig.headers["Authorization"] = `Bearer ${keycloak.token}`
            await axios.post(`${userApiUrl}/register`, keycloakUser, axiosConfig)
        } catch (err) {
            alert("Error while registering user: " + err)
        }
    }

    return (
        <div className="menu">
            <div className="menu-bg"> </div>
            <div className="nav-menu">
                <a href="/" className="nav-menu-logo"/>
            <div className="nav-menu-list">
                <div className="nav-menu-list-items">
                    <a href={appPageUrl} onClick={register}> APP </a>
                    <a href={accountUrl}> ACCOUNT </a>
                    <a href={adminUrl}> ADMIN CONSOLE </a>
                </div>
                <div className="buts">
                    {!keycloak.authenticated && (
                        <a href="#" className="btn btn-orange aa"
                           onClick={() => keycloak.login()}> LOGIN </a>
                    )}
                    {!!keycloak.authenticated && (
                        <a href="#" className="btn btn-orange aa"
                           onClick={() => keycloak.logout()}> LOGOUT </a>
                    )}
                </div>
            </div>
        </div>
        </div>
    );
}

export default HomeToolbar;