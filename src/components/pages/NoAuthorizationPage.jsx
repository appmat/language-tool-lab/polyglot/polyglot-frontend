import React from "react";
import keycloak from "../auth/Keycloak";
import {noAuthButtonStyle} from "../polyglot/polyglot_styles";

function NoAuthorizationPage() {
    return (
        <div className="no-auth-page">
            <div>
                <h1> Polyglot Auth Controller </h1>
                <button type="button" className="auth-button"
                        style={noAuthButtonStyle} o
                        nClick={() => keycloak.login()}>
                    Authorize
                </button>
            </div>
        </div>
    );
}

export default NoAuthorizationPage;