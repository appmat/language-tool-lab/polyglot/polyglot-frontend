import React from 'react';

const ErrorPage = () => {
    return (
        <div className="error-page">
            <div>
                <h1> Polyglot Controller </h1>
                <h1 style={{ color: "red" }}> Error </h1>
            </div>
        </div>
    );
}

export default ErrorPage;