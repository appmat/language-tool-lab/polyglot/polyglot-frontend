import Header from "../polyglot/header/Header";
import Body from "../polyglot/body/Body";
import React, {useState} from "react";
import '../../App.css';
import '../../assets/styles.css';
import {apiUrl, axiosConfig} from "../../config";
import {useKeycloak} from "@react-keycloak/web";
import axios from "axios";
import {getFreeId, getParagraphId} from "../polyglot/utils";
import {dataDivider} from "../../data";

function AppPage() {
    const [paragraphs, setParagraphs] = useState([]);
    const [notebooks, setNotebooks] = useState([]);
    const [notebookid, setNotebookid] = useState("0");
    const [kernelTasks, setKernelTasks] = useState([])
    let kernelTask = {}

    const { keycloak } = useKeycloak();

    async function onCreateParagraph(newParagraph) {
        axiosConfig.headers["Authorization"] = `Bearer ${keycloak.token}`
        await axios.post(`${apiUrl}/notebook/${notebookid}/paragraph/`, axiosConfig)
            .then(() => {
                setParagraphs(prevParagraphs => {
                    return [...prevParagraphs, newParagraph];
                });
            })
            .catch(err => {
                kernelTask[getFreeId(kernelTask)] = "Failed to create paragraph: " + err
                addKernelTask(kernelTask)
            })
    }

    async function onDeleteParagraph(pid) {
        axiosConfig.headers["Authorization"] = `Bearer ${keycloak.token}`
        await axios.delete(`${apiUrl}/notebook/${notebookid}/paragraph/${pid}`, axiosConfig)
            .then(() => {
                const newList = paragraphs
                    .filter((paragraph) => getParagraphId(paragraph) !== pid)
                setParagraphs(newList);
            })
            .catch(err => {
                kernelTask[getFreeId(kernelTask)] = "Failed to delete paragraph: " + err
                addKernelTask(kernelTask)
            })
    }

    async function onInsertParagraph(index, newParagraph) {
        axiosConfig.headers["Authorization"] = `Bearer ${keycloak.token}`
        await axios.post(`${apiUrl}/notebook/${notebookid}/paragraph/`, axiosConfig)
            .then(res => {
                let id = res.data
                newParagraph[id] = dataDivider
                setParagraphs(prevParagraphs => {
                    return [
                        ...prevParagraphs.slice(0, index),
                        newParagraph,
                        ...prevParagraphs.slice(index)
                    ]
                })
            })
            .catch(err => {
                kernelTask[getFreeId(kernelTask)] = "Failed to create paragraph: " + err
                addKernelTask(kernelTask)
            })
    }

    async function onSaveNotebook() {
        await axios.patch(`${apiUrl}/notebook/${notebookid}`, axiosConfig)
            .then(() => {})
            .catch(err => {
                kernelTask[getFreeId(kernelTask)] = "Failed to save notebook: " + err
                addKernelTask(kernelTask)
            })
    }

    const addKernelTask = (task) => {
        setKernelTasks(prevTask => {
            return [...prevTask, task]
        })
    }

    const removeKernelTask = (taskId) => {
        const newTaskList = kernelTasks.filter((task) => Object.keys(task)[0] !== taskId)
        setKernelTasks(newTaskList)
    }

    return (
        <div className="main-ui">
            <Header notebookid={notebookid}
                    notebooks={notebooks}
                    setNotebooks={setNotebooks}
                    paragraphs={paragraphs}
                    deleteParagraph={onDeleteParagraph}
                    insertParagraph={onInsertParagraph}
                    saveNotebook={onSaveNotebook}

                    kernelTasks={kernelTasks}
                    addKernelTask={addKernelTask}
            />
            <Body setNotebookid={setNotebookid}
                  notebookid={notebookid}
                  notebooks={notebooks}
                  setNotebooks={setNotebooks}
                  paragraphs={paragraphs}
                  setParagraphs={setParagraphs}
                  createParagraph={onCreateParagraph}
                  insertParagraph={onInsertParagraph}

                  kernelTasks={kernelTasks}
                  setKernelTasks={setKernelTasks}
                  addKernelTask={addKernelTask}
                  removeKernelTask={removeKernelTask}
            />
        </div>
    );
}

export default AppPage;