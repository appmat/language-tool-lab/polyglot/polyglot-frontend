import React from "react";
import { useKeycloak } from "@react-keycloak/web";
import NoAuthorizationPage from "../pages/NoAuthorizationPage";

const PrivateRoute = ({ children }) => {
    const {keycloak} = useKeycloak();
    const isLoggedIn = keycloak.authenticated;
    return isLoggedIn ? children : NoAuthorizationPage();
};

export default PrivateRoute;