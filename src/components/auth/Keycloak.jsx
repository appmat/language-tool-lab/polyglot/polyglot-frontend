import Keycloak from "keycloak-js";
import { keycloakRealm, keycloakClientId, keycloakUrl } from "../../config"

const keycloak = new Keycloak({
    "url": keycloakUrl,
    "realm": keycloakRealm,
    "clientId": keycloakClientId,
    "ssl-required": "external",
    "public-client": true,
    "confidential-port": 0
});

export default keycloak;