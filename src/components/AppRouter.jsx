import React from "react";
import { Route, Routes, Navigate } from "react-router-dom";
import {privateRoutes, publicRoutes} from "../router/router";
import {errorUrl} from "../config";
import PrivateRoute from "./auth/PrivateRoute";

function AppRouter() {
    return (
        <Routes>
            {privateRoutes.map(route =>
                    <Route path={route.path} exact={route.exact} element={
                        <PrivateRoute>
                            <route.element/>
                        </PrivateRoute>
                    } key={route} />
            )}
            {publicRoutes.map(route =>
                <Route
                    key={route}
                    element={<route.element/>}
                    path={route.path}
                    exact={route.exact}
                />
            )}
            <Route path="*" element={<Navigate to={errorUrl} replace />}/>
        </Routes>
    );
}

export default AppRouter;