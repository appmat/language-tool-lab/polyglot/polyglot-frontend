FROM node:18-alpine as build-deps

WORKDIR /web
COPY package.json .
RUN npm install
COPY . .
EXPOSE 3001
CMD ["npm", "start"]