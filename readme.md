## Build and run

React app:
- ```text
  make build-react-app
  ```

Keycloak:
- ```text
  make build-keycloak
  ```
  ```docker
  docker-compose -f docker-compose.yml up -d keycloack
  ```

Complete app:
- ```docker
  docker-compose up -d
  ```